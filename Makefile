build:
	pipenv install
	pipenv install --dev
	pipenv run pyinstaller \
		--name="double-down" \
		--add-data="assets:assets" \
		--onefile \
		src/main.py
	echo "Build complete"

install:
	install -m 644 assets/double-down.desktop /usr/share/applications/double-down.desktop
	install -m 644 assets/icon.png /usr/share/icons/double-down.png
	install -m 755 dist/double-down /usr/bin/double-down

clean:
	rm -rf build
	rm -rf dist
	rm -rf double-down.spec
	echo "Clean complete"
