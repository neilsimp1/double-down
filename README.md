# Double Down

## About
This is an unofficial sequel to the classic Falldown arcade game.
As I see it now, it exists in multiple forms on a few different modern platforms, but I am most familiar with the game from the TI-83 graphing calculator.
See https://yt.artemislena.eu/watch?v=GvO0s-x-A0s.

This game is created as part of the [Free-Software Purism Game Jam](https://itch.io/jam/free-software-purism-game-jam).
The theme of the game jam is "symbiotic".
This game goes with the theme by adding a second ball. If one dies, the other has a chance to revive it by collecting 3 stars as it falls.

## How to play
Don't let the balls touch the top of the screen! Use items to help you along the way.
For each ball alive on screen, you receive one point per second.

### Controls
On touchscreen, just tap and hold the lower left/right half of the screen to move the balls.
On keyboard, Left/Right arrow keys, `z` and `/`, and `h` and `l` will all move the balls left and right.

## How to build/install
You must have pipenv installed.

```
make
```
will enter a `pipenv shell`,  and use `pyinstaller` to create an executable under `dist/double-down`.
You can now run `double-down` from within this directory. 

```
sudo make install
```
will install the game to your system.

## Credits
- [pygame](https://github.com/pygame/pygame) and [pygame-menu](https://github.com/ppizarror/pygame-menu)
- KRxZA for https://www.looperman.com/loops/detail/341312/simple-bossa-loop-free-71bpm-samba-acoustic-guitar-loop
- EMPINHO for https://www.looperman.com/loops/detail/365055/kyoto-melody-keys-free-130bpm-rap-piano-loop
  - Vincent & The Black Rabbit for adding drums
