import pygame
from pygame.locals import (
	RLEACCEL,
)

COLOR_BLUE = (0, 0, 255)
COLOR_RED = (255, 0, 0)
COLOR_PURPLE = (128, 0, 128)

class Ball(pygame.sprite.Sprite):
	BLUE = 1
	RED = 2
	PURPLE = 3

	def __init__(self, type, radius, start_x, gamedata):
		super(Ball, self).__init__()
		self.image = pygame.Surface((radius * 2, radius * 2))
		self.image.fill((255, 255, 255))
		self.image.set_colorkey((255, 255, 255), RLEACCEL)
		pygame.draw.circle(self.image, self.get_color(type), (radius, radius), radius)
		pygame.draw.circle(self.image, (0, 0, 0), (radius, radius), radius, 2)
		self.rect = self.image.get_rect(midtop=(start_x, 20))

		self.type = type
		self.start_x = start_x
		self.gamedata = gamedata
		self.is_falling = True
		self.bounce = ""
		self.has_lightning = False

	def update(self, move_dir):
		self.rect.move_ip(self.get_speed(move_dir))

		# Keep ball on the screen, with a small gap at the bottom to allow room for your finger on touch-screen
		bottom_bound = self.gamedata.bottom_bound - self.gamedata.col_width
		if self.rect.left < self.gamedata.left_bound:
			self.rect.left = self.gamedata.left_bound
		elif self.rect.right > self.gamedata.right_bound:
			self.rect.right = self.gamedata.right_bound
		if self.rect.top <= self.gamedata.top_bound:
			self.rect.top = self.gamedata.top_bound
		elif self.rect.bottom >= bottom_bound:
			self.rect.bottom = bottom_bound

	def get_color(self, type):
		if type == Ball.BLUE:
			return COLOR_BLUE
		elif type == Ball.RED:
			return COLOR_RED
		else:
			return COLOR_PURPLE

	def get_speed(self, move_dir):
		y_speed = self.gamedata.fall_speed if self.is_falling else self.gamedata.rise_speed

		x_speed = 0
		x_speed_moving = self.gamedata.ball_x_speed if not self.has_lightning else self.gamedata.ball_x_speed + 5
		if self.bounce != "":
			x_speed = -15 if self.bounce == "left" else 15
			y_speed = -10
		elif move_dir == "left":
			x_speed = -x_speed_moving
		elif move_dir == "right":
			x_speed = x_speed_moving

		return (x_speed, y_speed)

	def is_on_top_of_block(self, block):
		ball_center_x = self.rect.center[0]
		ball_left_fall = ball_center_x + 6
		ball_right_fall = ball_center_x - 6

		# If ball is between the left/right sides of the block, offset by 6px so it can "roll" off the block on the edges
		if ball_left_fall > block.rect.left and ball_right_fall < block.rect.right:
			# If the ball is on top of the block
			if block.rect.top - 2 < self.rect.bottom <= block.rect.bottom:
				self.rect.bottom = block.rect.top
				return True

		return False

	def die(self):
		self.kill()
		self.rect.move(-self.gamedata.screen_width, 0)

	def revive(self):
		self.rect.midbottom = (self.start_x, self.gamedata.bottom_bound + self.gamedata.col_width)
