from item import Item
import os
import pygame
import random

class Star(Item):
	BLUE = 1
	RED = 2
	PURPLE = 3

	def __init__(self, type, gamedata):
		super().__init__(gamedata, self.get_filename(type, gamedata))
		pygame.sprite.Sprite.__init__(self)

		half_col_width = gamedata.col_width / 2
		start_y = -20 if type == Star.PURPLE else gamedata.screen_height + 20
		self.rect = self.image.get_rect(center = (
				random.randint(int(gamedata.left_bound + half_col_width), int(gamedata.right_bound - half_col_width)),
				start_y,
			)
		)

		self.gamedata = gamedata
		self.type = type
		self.speed = self.get_speed(gamedata)

	def update(self):
		self.rect.move_ip(0, self.speed)
		if self.type == Star.PURPLE:
			if self.rect.top > self.gamedata.game_height: self.kill()
		else:
			if self.rect.bottom < 0: self.kill()

	def get_speed(self, gamedata):
		speed = 0
		if gamedata.screen_height <= 720: speed = random.randint(-8, -3)
		elif gamedata.screen_height <= 900: speed = random.randint(-10, -6)
		else: speed = random.randint(-12, -6)

		if self.type == Star.PURPLE:
			speed = (speed + 2) * -1
		
		return speed
	
	def get_filename(self, type, gamedata):
		if type == Star.BLUE:
			# return Path.joinpath(gamedata.assets_dir, "star_blue.png")
			return os.path.join(gamedata.assets_dir, "star_blue.png")
		elif type == Star.RED:
			# return Path.joinpath(gamedata.assets_dir, "star_red.png")
			return os.path.join(gamedata.assets_dir, "star_red.png")
		else:
			# return Path.joinpath(gamedata.assets_dir, "star_purple.png")
			return os.path.join(gamedata.assets_dir, "star_purple.png")
