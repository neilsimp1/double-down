import pygame

class GameData():
	def __init__(self, assets_dir, audio):
		self.assets_dir = assets_dir
		self.audio = audio

		# Fullscreen, but with the game area in a 1:2 aspect ratio to match the L5's 720x1440 (320x720 scaled)
		display_info = pygame.display.Info()
		self.screen_width = display_info.current_w
		self.screen_height = display_info.current_h
		self.game_width = display_info.current_h / 2
		self.game_height = self.game_width * 2
		self.game_x = (self.screen_width / 2) - (self.game_width / 2)
		self.game_y = 0
		self.left_bound = self.game_x
		self.right_bound = self.game_x + self.game_width
		self.top_bound = self.game_y
		self.bottom_bound = self.game_y + self.game_height

		self.num_columns = 16
		self.col_width = self.game_width / self.num_columns
		
		self.items_to_revive = 3
		(self.fall_speed, self.rise_speed, self.ball_x_speed, self.init_time_block_row) = self.get_starting_difficulties(self.screen_height)
		self.time_block_row = self.init_time_block_row

		self.time_point = 1000
		self.time_fg = 650
		self.time_star = 3000
		self.time_star_purple = 120000
		self.time_lightning = 30000
		self.time_increase_rise_speed = 50000
		self.time_increase_block_row_speed = 45000
		self.time_block_pattern = 65000
		self.time_change_music = 180000

	def get_starting_difficulties(self, screen_height):
		if screen_height <= 720:
			return (12, -3, 6, 400)
		elif screen_height <= 900:
			return (14, -5, 6, 390)
		else:
			return (16, -6, 7, 380)

	def hard_mode(self):
		self.time_block_row -= 10
