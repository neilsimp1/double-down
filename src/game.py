from background import get_bg_color
from ball import Ball
from block import Block
from foreground import Foreground
from lightning import Lightning
from patterns import patterns
from points import Points
import pygame
from pygame.locals import (
	K_LEFT,
	K_RIGHT,
	USEREVENT,
	K_h,
	K_l,
	K_z,
	K_SLASH,
	K_q,
	K_ESCAPE,
	KEYDOWN,
	QUIT,
)
import random
from star import Star

class Game():
	def __init__(self, gamedata, screen, hard_mode):
		self.gamedata = gamedata
		self.gamedata.time_block_row = self.gamedata.init_time_block_row
		if (hard_mode):
			gamedata.hard_mode()

		self.screen = screen
		self.clock = pygame.time.Clock()

		self.item_count = 0
		self.point_count = 0
		self.next_pattern = ""

		self.ball_one = Ball(
			Ball.BLUE,
			self.gamedata.col_width / 2,
			self.gamedata.left_bound + (self.gamedata.game_width / 3),
			self.gamedata
		)
		self.ball_two = Ball(
			Ball.RED,
			self.gamedata.col_width / 2,
			self.gamedata.right_bound - (self.gamedata.game_width / 3),
			self.gamedata
		)
		self.ball_three = Ball(
			Ball.PURPLE,
			self.gamedata.col_width / 2,
			self.gamedata.left_bound + (self.gamedata.game_width / 2),
			self.gamedata
		)

		self.points = Points(self.gamedata)

		self.blocks = pygame.sprite.Group()
		self.foreground = pygame.sprite.Group()
		self.lightnings = pygame.sprite.Group()
		self.stars = pygame.sprite.Group()
		self.purple_stars = pygame.sprite.Group()
		self.all_sprites = pygame.sprite.Group()
		self.all_sprites.add(self.ball_one)
		self.all_sprites.add(self.ball_two)
		self.all_sprites.add(self.blocks)
		self.all_sprites.add(self.foreground)
		self.all_sprites.add(self.lightnings)
		self.all_sprites.add(self.stars)
		self.all_sprites.add(self.points)
		self.all_sprites.add(self.purple_stars)

	def init_events(self):
		self.ADD_POINT = pygame.USEREVENT + 1
		pygame.time.set_timer(self.ADD_POINT, self.gamedata.time_point)

		self.ADD_BLOCK_ROW = pygame.USEREVENT + 2
		pygame.time.set_timer(self.ADD_BLOCK_ROW, self.gamedata.time_block_row)

		self.INCREASE_BLOCK_ROW_SPEED = USEREVENT + 3
		pygame.time.set_timer(self.INCREASE_BLOCK_ROW_SPEED, self.gamedata.time_increase_block_row_speed)

		self.ADD_FG = pygame.USEREVENT + 4
		pygame.time.set_timer(self.ADD_FG, self.gamedata.time_fg)
		
		self.ADD_STAR = pygame.USEREVENT + 5
		pygame.time.set_timer(self.ADD_STAR, self.gamedata.time_star)

		self.ADD_LIGHTNING = pygame.USEREVENT + 6
		pygame.time.set_timer(self.ADD_LIGHTNING, self.gamedata.time_lightning)

		self.INCREASE_RISE_SPEED = pygame.USEREVENT + 7
		pygame.time.set_timer(self.INCREASE_RISE_SPEED, self.gamedata.time_increase_rise_speed)

		self.ADD_PURPLE_STAR = pygame.USEREVENT + 8
		pygame.time.set_timer(self.ADD_PURPLE_STAR, self.gamedata.time_star_purple)

		self.BLOCK_PATTERN = pygame.USEREVENT + 9
		pygame.time.set_timer(self.BLOCK_PATTERN, self.gamedata.time_block_pattern)

		self.CHANGE_MUSIC_FADEOUT = pygame.USEREVENT + 10
		pygame.time.set_timer(self.CHANGE_MUSIC_FADEOUT, self.gamedata.time_change_music, loops=1)

		self.CHANGE_MUSIC_NEXT = pygame.USEREVENT + 11
		self.DECREASE_RISE_SPEED = pygame.USEREVENT + 12
		self.CLEAR_BOUNCE_ANIM = pygame.USEREVENT + 13
		self.END_LIGHTNING = pygame.USEREVENT + 14

	def run(self):
		self.running = True
		self.init_events()

		while self.running:
			self.handle_events()

			if not self.ball_one.alive() and not self.ball_two.alive() and not self.ball_three.alive():
				self.running = False
			else:
				pressed_keys = pygame.key.get_pressed()
				mouse_left_or_right = ""
				if pygame.mouse.get_pressed()[0] == True:
					mouse_left_or_right = self.is_mouseclick_in_left_or_right()

				move_dir = ""
				if self.is_left_key(pressed_keys) or mouse_left_or_right == "left":
					move_dir = "left"
				elif self.is_right_key(pressed_keys) or mouse_left_or_right == "right":
					move_dir = "right"

				self.ball_one.update(move_dir)
				self.ball_two.update(move_dir)
				self.ball_three.update(move_dir)
				self.blocks.update()
				self.foreground.update()
				self.stars.update()
				self.lightnings.update()
				self.points.update(self.point_count)
				self.purple_stars.update()

				self.check_collisions()
				self.check_ball_revive()

			self.paint()

			self.clock.tick(60)

	def handle_events(self):
		for event in pygame.event.get():
			if (event.type == KEYDOWN and (event.key == K_ESCAPE or event.key == K_q)) or event.type == QUIT:
				self.running = False
			elif event.type == self.ADD_POINT:
				self.point_count += self.get_points_to_add()

			elif event.type == self.ADD_BLOCK_ROW:
				pattern = ""
				if self.next_pattern != "":
					pattern = self.next_pattern
					self.next_pattern = ""
				block_row = Block.new_block_row(self.gamedata, pattern)
				self.blocks.add(block_row)
				self.all_sprites.add(block_row)
			elif event.type == self.INCREASE_BLOCK_ROW_SPEED:
				pygame.time.set_timer(self.ADD_BLOCK_ROW, 0)
				self.gamedata.time_block_row -= 5
				pygame.time.set_timer(self.ADD_BLOCK_ROW, self.gamedata.time_block_row)
			elif event.type == self.BLOCK_PATTERN:
				self.set_block_pattern()

			elif event.type == self.ADD_FG:
				fg = Foreground(self.gamedata)
				self.foreground.add(fg)
				self.all_sprites.add(fg)
			elif event.type == self.ADD_STAR and (self.ball_one.alive() ^ self.ball_two.alive()):
				star_type = Star.RED if self.ball_one.alive() else Star.BLUE
				star = Star(star_type, self.gamedata)
				self.stars.add(star)
				self.all_sprites.add(star)
			elif event.type == self.ADD_PURPLE_STAR and not self.ball_three.alive():
				star = Star(Star.PURPLE, self.gamedata)
				self.purple_stars.add(star)
				self.all_sprites.add(star)
			elif event.type == self.ADD_LIGHTNING:
				lightning = Lightning(self.gamedata)
				self.lightnings.add(lightning)
				self.all_sprites.add(lightning)
			elif event.type == self.END_LIGHTNING:
				self.ball_one.has_lightning = self.ball_two.has_lightning = self.ball_three.has_lightning = False

			elif event.type == self.INCREASE_RISE_SPEED:
				self.gamedata.rise_speed += 1
				duration = 10000 if self.point_count < 150 else 20000
				pygame.time.set_timer(self.DECREASE_RISE_SPEED, duration, loops=1)
				self.gamedata.audio.clock.play()
			elif event.type == self.DECREASE_RISE_SPEED:
				self.gamedata.rise_speed -= 1

			elif event.type == self.CLEAR_BOUNCE_ANIM:
				self.ball_one.bounce = self.ball_two.bounce = self.ball_three.bounce = ""

			elif event.type == self.CHANGE_MUSIC_FADEOUT:
				if not self.gamedata.audio.notch_already_kicked_up:
					self.gamedata.audio.fadeout()
					pygame.time.set_timer(self.CHANGE_MUSIC_NEXT, 5000, loops=1)
			elif event.type == self.CHANGE_MUSIC_NEXT:
				self.gamedata.audio.kick_it_up_a_notch(self.gamedata.assets_dir)

	def check_collisions(self):
		# Check if balls are falling or are on a block
		self.ball_one.is_falling = True
		for block in pygame.sprite.spritecollide(self.ball_one, self.blocks, 0):
			self.ball_one.is_falling = not self.ball_one.is_on_top_of_block(block)
		self.ball_two.is_falling = True
		for block in pygame.sprite.spritecollide(self.ball_two, self.blocks, 0):
			self.ball_two.is_falling = not self.ball_two.is_on_top_of_block(block)
		self.ball_three.is_falling = True
		for block in pygame.sprite.spritecollide(self.ball_three, self.blocks, 0):
			self.ball_three.is_falling = not self.ball_three.is_on_top_of_block(block)

		# If balls touch, they should bounce away
		did_collide = False
		if self.ball_one.alive() and self.ball_two.alive():
			if pygame.sprite.collide_rect(self.ball_one, self.ball_two):
				if self.ball_one.rect.left < self.ball_two.rect.left:
					self.gamedata.audio.bounce1.play()
					self.ball_one.bounce = "left"
					self.ball_two.bounce = "right"
				else:
					self.gamedata.audio.bounce2.play()
					self.ball_one.bounce = "right"
					self.ball_two.bounce = "left"
				did_collide = True
		if self.ball_three.alive() and self.ball_one.alive():
			if pygame.sprite.collide_rect(self.ball_one, self.ball_three):
				if self.ball_one.rect.left < self.ball_three.rect.left:
					self.gamedata.audio.bounce1.play()
					self.ball_one.bounce = "left"
					self.ball_three.bounce = "right"
				else:
					self.gamedata.audio.bounce3.play()
					self.ball_one.bounce = "right"
					self.ball_three.bounce = "left"
				did_collide = True
		if self.ball_three.alive() and self.ball_two.alive():
			if pygame.sprite.collide_rect(self.ball_two, self.ball_three):
				if self.ball_two.rect.left < self.ball_three.rect.left:
					self.gamedata.audio.bounce3.play()
					self.ball_two.bounce = "left"
					self.ball_three.bounce = "right"
				else:
					self.gamedata.audio.bounce2.play()
					self.ball_two.bounce = "right"
					self.ball_three.bounce = "left"
				did_collide = True
		if did_collide:
			pygame.time.set_timer(self.CLEAR_BOUNCE_ANIM, 100, loops=1)

		# If a ball passes the top of the screen
		if self.ball_one.rect.top == 0 and self.ball_one.alive():
			self.gamedata.audio.death1.play()
			self.ball_one.die()
		if self.ball_two.rect.top == 0 and self.ball_two.alive():
			self.gamedata.audio.death2.play()
			self.ball_two.die()
		if self.ball_three.rect.top == 0 and self.ball_three.alive():
			self.gamedata.audio.death3.play()
			self.ball_three.die()

		# If a ball touches a star
		if (self.ball_one.alive() and pygame.sprite.spritecollide(self.ball_one, self.stars, 1)) or (self.ball_two.alive() and pygame.sprite.spritecollide(self.ball_two, self.stars, 1)):
			self.handle_star()

		# If a ball touches a purple star
		if (self.ball_one.alive() and pygame.sprite.spritecollide(self.ball_one, self.purple_stars, 1)) or (self.ball_two.alive() and pygame.sprite.spritecollide(self.ball_two, self.purple_stars, 1)):
			self.handle_purple_star()

		# If a ball touches lightning
		if self.ball_one.alive() and pygame.sprite.spritecollide(self.ball_one, self.lightnings, 1):
			self.handle_lightning(self.ball_one)
		elif self.ball_two.alive() and pygame.sprite.spritecollide(self.ball_two, self.lightnings, 1):
			self.handle_lightning(self.ball_two)
		elif self.ball_three.alive() and pygame.sprite.spritecollide(self.ball_three, self.lightnings, 1):
			self.handle_lightning(self.ball_three)
		
	def paint(self):
		self.screen.fill(get_bg_color(self.point_count))

		if self.gamedata.screen_width > self.gamedata.game_width:
			pygame.draw.rect(self.screen, (0, 0, 0), (self.gamedata.left_bound -5, 0, 5, self.gamedata.game_height), 2)
			pygame.draw.rect(self.screen, (0, 0, 0), (self.gamedata.right_bound, 0, 5, self.gamedata.game_height), 2)

		for sprite in self.all_sprites:
			self.screen.blit(sprite.image, sprite.rect)

		pygame.display.flip()

	def is_left_key(self, pressed_keys):
		return True if pressed_keys[K_LEFT] or pressed_keys[K_h] or pressed_keys[K_z] else False

	def is_right_key(self, pressed_keys):
		return True if pressed_keys[K_RIGHT] or pressed_keys[K_l] or pressed_keys[K_SLASH] else False
	
	def is_mouseclick_in_left_or_right(self):
		position = pygame.mouse.get_pos()
		if position[0] < self.gamedata.screen_width / 2 and position[1] >= self.gamedata.screen_height / 2:
			return "left"
		elif position[0] >= self.gamedata.screen_width / 2 and position[1] >= self.gamedata.screen_height / 2:
			return "right"
		else:
			return ""

	def check_ball_revive(self):
		if self.item_count == self.gamedata.items_to_revive:
			if self.ball_one.alive():
				self.ball_two.revive()
				self.all_sprites.add(self.ball_two)
			elif self.ball_two.alive():
				self.ball_one.revive()
				self.all_sprites.add(self.ball_one)
			self.item_count = 0
			self.gamedata.audio.revive.play()

	def handle_star(self):
		self.item_count += 1
		if self.item_count == 1:
			self.gamedata.audio.star1.play()
		elif self.item_count == 2:
			self.gamedata.audio.star2.play()
		elif self.item_count == 3:
			self.gamedata.audio.star3.play()

	def handle_purple_star(self):
		self.ball_three.revive()
		self.all_sprites.add(self.ball_three)
		self.gamedata.audio.purple_star.play()

	def handle_lightning(self, ball):
		ball.has_lightning = True
		pygame.time.set_timer(self.END_LIGHTNING, 5000, loops=1)
		self.gamedata.audio.whoosh.play()

	def set_block_pattern(self):
		self.next_pattern = random.choice(list(patterns.keys()))

	def get_points_to_add(self):
		points_to_add = 0
		if self.ball_one.alive(): points_to_add += 1
		if self.ball_two.alive(): points_to_add += 1
		if self.ball_three.alive(): points_to_add += 1
		return points_to_add
