from highscore import HighScore
import options
import os
import pygame
import pygame_menu

menu_theme = pygame_menu.Theme(
	background_color = (0, 0, 0),
	title_background_color = (0, 0, 0),
	title_close_button = False,
	title_font_color = (255, 255, 255),
	title_font = "monospace",
	widget_font = "monospace",
	widget_padding = 25,
)

GRAY_BG = (20, 20 ,20)
WHITE_FONT = (240, 240, 240)
GRAY_FONT = (200, 200, 200)

def main_menu(gamedata, play_game):
	main_menu = pygame_menu.Menu("", gamedata.screen_width, gamedata.screen_height, theme=menu_theme, verbose=False)
	main_menu.add.label("Double Down", background_color=GRAY_BG, font_color=WHITE_FONT, font_size=46, margin=(0, 32))
	main_menu.add.button("Play", play_game)
	main_menu.add.button("How to play", how_to_play_menu(gamedata))
	main_menu.add.button("Options", options_menu(gamedata))
	main_menu.add.button("Quit", pygame_menu.events.EXIT)
	return main_menu

def how_to_play_menu(gamedata):
	header_size = int(gamedata.col_width * 1.2)
	font_size = int(gamedata.col_width * .8)
	how_to_play_menu = pygame_menu.Menu("", gamedata.screen_width, gamedata.screen_height, theme=menu_theme, verbose=False)
	how_to_play_menu.add.label("Object:", background_color=GRAY_BG, font_color=WHITE_FONT, font_size=header_size, max_char=-1, padding=12)
	how_to_play_menu.add.label("", padding=1)
	how_to_play_menu.add.label("Don't let the balls touch the top of the screen! Use items to help you along the way.", font_color=GRAY_FONT, font_size=font_size, max_char = -1, padding=0)
	how_to_play_menu.add.label("")
	how_to_play_menu.add.label("Controls:", background_color=GRAY_BG, font_color=WHITE_FONT, font_size=header_size, max_char=-1, padding=12)
	how_to_play_menu.add.label("", padding=1)
	how_to_play_menu.add.label("On touchscreen, hold down on the lower left/right of the screen to move.", font_color=GRAY_FONT, font_size=font_size, max_char=-1, padding=0)
	how_to_play_menu.add.label("", padding=1)
	how_to_play_menu.add.label("On keyboard, Left/Right arrows, `z` and `/`, and `h` and `l` will move the balls.", font_color=GRAY_FONT, font_size=font_size, max_char=-1, padding=0)
	how_to_play_menu.add.label("", padding=4)
	how_to_play_menu.add.button("Back", pygame_menu.events.BACK)
	return how_to_play_menu

def options_menu(gamedata):
	header_size = int(gamedata.col_width * 1.2)
	font_size = int(gamedata.col_width * .8)
	slider_color = GRAY_FONT
	state_color = ((0, 0, 0), GRAY_BG)
	options_menu = pygame_menu.Menu("", gamedata.screen_width, gamedata.screen_height, theme=menu_theme, verbose=False)
	options_menu.add.label("Options:", background_color=GRAY_BG, font_color=WHITE_FONT, font_size=header_size, max_char=-1, padding=12)
	options_menu.add.label("", padding=1)
	options_menu.add.toggle_switch("Music", options.music_enabled, options_set_music_on, font_size=font_size, slider_color=slider_color, state_color=state_color)
	options_menu.add.toggle_switch("Hard Mode", options.hard_mode, options_set_hard_mode, font_size=font_size, slider_color=slider_color, state_color=state_color)
	options_menu.add.label("", padding=4)
	options_menu.add.button("High Scores", highscore_menu(gamedata, 0, False))
	options_menu.add.button("Back", pygame_menu.events.BACK)
	return options_menu

def options_set_music_on(value):
	options.music_enabled = value
	if value:
		pygame.mixer.music.unpause()
	else:
		pygame.mixer.music.pause()

def options_set_hard_mode(value):
	options.hard_mode = value

def score_menu(gamedata, points_count):
	score_display = pygame_menu.Menu("", gamedata.screen_width, gamedata.screen_height, theme=menu_theme, verbose=False)
	score_display.add.label(str(points_count), background_color=GRAY_BG, font_color=WHITE_FONT, font_size=60)
	score_display.add.label("Points", font_size=46, margin=(0, 60))
	score_display.add.button("Ok", score_display.disable)
	return score_display

def highscore_menu(gamedata, points_count, allow_input=True):
	def save_high_score():
		name = highscore_display.get_widget("name_input").get_value()
		if not name:
			gamedata.audio.whoosh.play()
			return
		HighScore.save_highscore(name, points_count)
		gamedata.audio.revive.play()
		highscore_display.disable()

	username = os.getlogin()
	font_size = int(gamedata.col_width * .8)
	font_size_scores = int(font_size * .9)
	highscore_display = pygame_menu.Menu("", gamedata.screen_width, gamedata.screen_height, theme=menu_theme, verbose=False)

	if allow_input:
		highscore_display.add.text_input("Name: ", textinput_id="name_input", default=username, font_size=font_size, maxchar=14)
		highscore_display.add.button("Submit High Score", save_high_score, font_size=font_size)
	
	label = " #  Pts  Name            Date      \n"
	for i, highscore in enumerate(HighScore.load_highscores()):
		number = f"{i + 1}:".rjust(3, " ")
		score = str(highscore.score).ljust(4, " ")
		name = highscore.name.ljust(15, " ") if len(highscore.name) < 15 else highscore.name[:12] + "..."
		date = highscore.datetime.strftime("%Y-%m-%d")
		label += f"{number} {score} {name} {date}\n"
	highscore_display.add.label(label, font_size=font_size_scores, padding=2)

	highscore_display.add.button("Back", highscore_display.disable if allow_input else pygame_menu.events.BACK)
	
	return highscore_display
