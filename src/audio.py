import os
import pygame

class Audio():
	def __init__(self, assets_dir):
		pygame.mixer.init()

		# https://www.looperman.com/loops/detail/341312/simple-bossa-loop-free-71bpm-samba-acoustic-guitar-loop
		pygame.mixer.music.load(os.path.join(assets_dir, "simple-bossa-loop.ogg"))
		pygame.mixer.music.play(loops=-1)

		self.bounce1 = pygame.mixer.Sound(os.path.join(assets_dir, "bounce1.ogg"))
		self.bounce2 = pygame.mixer.Sound(os.path.join(assets_dir, "bounce2.ogg"))
		self.bounce3 = pygame.mixer.Sound(os.path.join(assets_dir, "bounce3.ogg"))
		self.clock = pygame.mixer.Sound(os.path.join(assets_dir, "clock.ogg"))
		self.death1 = pygame.mixer.Sound(os.path.join(assets_dir, "death1.ogg"))
		self.death2 = pygame.mixer.Sound(os.path.join(assets_dir, "death2.ogg"))
		self.death3 = pygame.mixer.Sound(os.path.join(assets_dir, "death3.ogg"))
		self.purple_star = pygame.mixer.Sound(os.path.join(assets_dir, "purple_star.ogg"))
		self.star1 = pygame.mixer.Sound(os.path.join(assets_dir, "star1.ogg"))
		self.star2 = pygame.mixer.Sound(os.path.join(assets_dir, "star2.ogg"))
		self.star3 = pygame.mixer.Sound(os.path.join(assets_dir, "star3.ogg"))
		self.revive = pygame.mixer.Sound(os.path.join(assets_dir, "revive.ogg"))
		self.whoosh = pygame.mixer.Sound(os.path.join(assets_dir, "whoosh.ogg"))

		self.notch_already_kicked_up = False

	def fadeout(self):
		pygame.mixer.music.fadeout(5000)
		
	def kick_it_up_a_notch(self, assets_dir):
		pygame.mixer.music.stop()
		# https://www.looperman.com/loops/detail/365055/kyoto-melody-keys-free-130bpm-rap-piano-loop
		# and Vincent & The Black Rabbit
		pygame.mixer.music.load(os.path.join(assets_dir, "kyoto-melody-keys.ogg"))
		pygame.mixer.music.play(loops=-1)
		self.notch_already_kicked_up = True

	def stop(self):
		pygame.mixer.music.stop()
		pygame.mixer.quit()
