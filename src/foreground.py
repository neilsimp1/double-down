import os
import pygame
from pygame.locals import (
	RLEACCEL,
)
import random

class Foreground(pygame.sprite.Sprite):
	def __init__(self, gamedata):
		super(Foreground, self).__init__()
		self.image = pygame.image.load(os.path.join(gamedata.assets_dir, f"fg{random.randint(1, 3)}.png")).convert()
		self.image.set_colorkey((0, 0, 0), RLEACCEL)
		self.rect = self.image.get_rect(
			center=(
				random.randint(20, gamedata.screen_width - 20),
				gamedata.screen_height + 20,
			)
		)
		self.speed = random.randint(-8, -4)

	def update(self):
		self.rect.move_ip(0, self.speed)
		if self.rect.bottom < 0:
			self.kill()
