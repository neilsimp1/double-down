from patterns import patterns
import pygame
from pygame.locals import (
	RLEACCEL,
)
import random

class Block(pygame.sprite.Sprite):
	pattern_name = ""
	pattern_index = 0

	def __init__(self, color, x, width, gamedata):
		super(Block, self).__init__()
		rect = pygame.Rect(
			x + gamedata.left_bound + 4,
			gamedata.screen_height + 20,
			width - 8,
			gamedata.col_width
		)
		self.image = pygame.Surface((width - 8, gamedata.col_width))
		self.image.fill((255, 255, 255))
		self.image.set_colorkey((255, 255, 255), RLEACCEL)
		self.rect = self.image.get_rect()
		pygame.draw.rect(self.image, color, self.rect, 0, -1, 6, 6, 12, 12)
		pygame.draw.rect(self.image, (0, 0, 0), self.rect, 2, -1, 6, 6, 12, 12)
		self.rect.topleft = rect.topleft

		self.gamedata = gamedata
		
	def update(self):
		self.rect.move_ip(0, self.gamedata.rise_speed)
		if self.rect.bottom < 0:
			self.kill()

	# By default, will return a randomly generated row of blocks
	# If a pattern name is passed in, it will return rows from that pattern until it's complete, and then will switch back to random
	@staticmethod
	def new_block_row(gamedata, pattern=""):
		block_row = pygame.sprite.Group()
		rand_rgb = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

		columns = []
		if pattern != "" or Block.pattern_name != "":
			if pattern != "":
				Block.pattern_name = pattern

			columns = Block.get_pattern_columns()
			Block.pattern_index += 1

			if Block.pattern_index == len(patterns[Block.pattern_name]):
				Block.pattern_name = ""
				Block.pattern_index = 0
		else:
			columns = Block.get_random_columns(gamedata)

		columns.append(False) # Add False to the end to simplify the loop below, now it will always end on `not is_block`

		is_block = False
		x1 = x2 = 0
		block_x_coords = []
		for i in range(gamedata.num_columns + 1): # Loop once more and create an array of x coords to use to create the blocks
			if columns[i] and not is_block:
				is_block = True
				x1 = gamedata.col_width * i
			elif not columns[i] and is_block:
				is_block = False
				x2 = (gamedata.col_width * i)
				block_x_coords.append((x1, x2))

		for x_coords in block_x_coords: # Finally, create the blocks
			block = Block(rand_rgb, x_coords[0], x_coords[1] - x_coords[0], gamedata)
			block_row.add(block)

		return block_row

	@staticmethod
	def get_random_columns(gamedata):
		columns = [True for _ in range(gamedata.num_columns)] # Create an array of `True`s
		num_blanks_in_row = random.randint(2, 5) # How many spots in the row for the balls to fall through
		num_blanked = 0
		while num_blanked < num_blanks_in_row: # Loop until enough columns are randomly blanked out.
			i = random.randint(0, gamedata.num_columns - 1)
			if columns[i]:
				columns[i] = False
				num_blanked += 1
		return columns
	
	@staticmethod
	def get_pattern_columns():
		str = patterns[Block.pattern_name][Block.pattern_index]
		return list(map(lambda x: True if x == "1" else False, list(str)))
