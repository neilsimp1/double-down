from item import Item
import os
import pygame
import random

class Lightning(Item):
	def __init__(self, gamedata):
		super().__init__(gamedata, os.path.join(gamedata.assets_dir, "lightning.png"))
		pygame.sprite.Sprite.__init__(self)
		self.gamedata = gamedata
		self.speed = self.get_speed(gamedata)

		half_col_width = gamedata.col_width / 2
		self.rect = self.image.get_rect(center = (
				random.randint(int(gamedata.left_bound + half_col_width), int(gamedata.right_bound - half_col_width)),
				-20,
			)
		)

	def update(self):
		self.rect.move_ip(0, self.speed)
		if self.rect.top > self.gamedata.game_height:
			self.kill()

	def get_speed(self, gamedata):
		if gamedata.screen_height <= 720:
			return random.randint(3, 7)
		elif gamedata.screen_height <= 900:
			return random.randint(4, 8)
		else:
			return random.randint(5, 11)
