import colorsys
import random

START_BG_COLOR = 203
COLOR_INCREMENT = random.randint(2, 6)

def get_bg_color(num):
	num = num % 360
	h = (num * COLOR_INCREMENT) + START_BG_COLOR
	h = h if h <= 360 else h - 360
	(r, g, b) = colorsys.hls_to_rgb(h / 360, .92, .75)
	return (r * 255, g * 255, b * 255)
