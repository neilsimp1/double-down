import datetime
import os

DIR_PATH = os.path.join(os.environ["HOME"], ".local/share/double-down")
FILE_PATH = os.path.join(DIR_PATH, "highscores")
os.makedirs(DIR_PATH, exist_ok=True)

class HighScore():
	def __init__(self, name, score, datetime):
		self.name = name
		self.score = score
		self.datetime = datetime

	@staticmethod
	def load_highscores():
		highscores = []
		
		try:
			if not os.path.exists(FILE_PATH):
				return highscores

			with open(FILE_PATH, "r") as file:
				lines = file.readlines()
				for line in lines:
					try:
						highscore = HighScore.load_highscore(line.replace("\n", ""))
						highscores.append(highscore)
					except Exception:
						print(f"Error parsing high score from {FILE_PATH}: {line}")
		except Exception:
			print(f"Error parsing high scores from {FILE_PATH}")
		
		return highscores

	@staticmethod
	def load_highscore(line):
		[score, dt, name] = line.split("\t")
		return HighScore(name, int(score), datetime.datetime.fromisoformat(dt))

	@staticmethod
	def save_highscore(name, score):
		new_highscore = HighScore(name.rstrip(), score, datetime.datetime.now())
		highscores = HighScore.load_highscores()

		found = False
		for i, highscore in enumerate(highscores):
			if score > highscore.score:
				highscores.insert(i, new_highscore)
				found = True
				break
		if not found:
			highscores.append(new_highscore)
		if len(highscores) > 10:
			highscores = highscores[:10]

		lines = []
		for highscore in highscores:
			lines.append(f"{highscore.score}\t{highscore.datetime}\t{highscore.name}\n")

		try:
			with open(FILE_PATH, "w") as file:
				file.writelines(lines)
		except Exception:
			print(f"Error writing highscore file at {FILE_PATH}")

	@staticmethod
	def is_high_score(score):
		try:
			highscores = HighScore.load_highscores()
			if len(highscores) == 0:
				return True
			for highscore in highscores:
				if score > highscore.score:
					return True
			return False
		except Exception:
			return True
