#!/usr/bin/env python

from audio import Audio
from game import Game
from gamedata import GameData
from highscore import HighScore
from menu import ( highscore_menu, main_menu, score_menu )
import options
import os
import pygame
import sys

try:
	# PyInstaller creates a temp folder and stores path in _MEIPASS
	root_dir = sys._MEIPASS # type: ignore 
except Exception:
	root_dir = os.path.abspath(".")
assets_dir =  os.path.join(root_dir, "assets/")

gamedata = screen = None

def main():
	global gamedata, screen

	options.init()
	pygame.init()

	audio = Audio(assets_dir)
	gamedata = GameData(assets_dir, audio)
	screen = pygame.display.set_mode((gamedata.screen_width, gamedata.screen_height), pygame.FULLSCREEN|pygame.NOFRAME)
	
	menu = main_menu(gamedata, play_game)
	menu.mainloop(screen)
	audio.stop()

def play_game():
	global gamedata, screen

	game = Game(gamedata, screen, options.hard_mode)
	game.run()

	score_display = score_menu(gamedata, game.point_count)
	score_display.mainloop(screen)
	if HighScore.is_high_score(game.point_count):
		highscore_display = highscore_menu(gamedata, game.point_count)
		highscore_display.mainloop(screen)

def run():
	main()

if __name__ == "__main__":
	run()
