import pygame
from pygame.locals import (
	RLEACCEL,
)
import random

class Item(pygame.sprite.Sprite):
	def __init__(self, gamedata, filename):
		super(pygame.sprite.Sprite, self).__init__()
		self.image = pygame.image.load(filename).convert_alpha()
		self.image = pygame.transform.scale(self.image, (gamedata.col_width * 2, gamedata.col_width * 2))
		self.image.set_colorkey((255, 255, 255), RLEACCEL)
		self.rect = self.image.get_rect()
