import pygame
from pygame.locals import (
	RLEACCEL,
)

class Points(pygame.sprite.Sprite):
	def __init__(self, gamedata):
		super(Points, self).__init__()

		rect = pygame.Rect(
			gamedata.screen_width - (gamedata.col_width * 6),
			gamedata.col_width,
			gamedata.col_width * 4,
			gamedata.col_width * 2
		)
	
		self.image = pygame.Surface((gamedata.col_width * 5, gamedata.col_width * 2))
		self.image.fill((255, 255, 255))
		self.image.set_colorkey((255, 255, 255), RLEACCEL)
		self.rect = self.image.get_rect()
		self.rect.topleft = rect.topleft

		self.font = pygame.font.SysFont("monospace", int(gamedata.col_width * 2), True)
		self.color = (0, 0, 0)
		self.text = self.font.render("0", 1, self.color)
		text_rect = self.text.get_rect()
		text_rect.center = self.rect.center
		self.rect = text_rect

	def update(self, points):
		self.text = self.font.render(str(points), 1, self.color)
		self.image.fill((255, 255, 255))
		self.image.blit(self.text, self.text.get_rect())
